*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] and [R][R @ wikipedia] code to

1. "reunit" the data from [CLM-CN-N2O][]'s `mgN/m^2` to `molN2O/s` (what CMAQ wants).
1. 2D-regrid the data (in [netCDF][] format) from global/unprojected to a projected subdomain ([AQMEII-NA][]).
1. "retemporalize" from monthly timestep to hourly, creating CMAQ-style emissions files (e.g., [this][emis_mole_N2O_200801_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz], when `gunzip`ed) containing hourly emissions usable for any day in the month labeled (e.g., `200801` in the link).
1. check that mass is (more or less) conserved in all of the above

Currently does not provide a clean or general-purpose (much less packaged) solution! This project merely shows how to perform these tasks using

* bash (tested with version=3.2.25)
* NCL (tested with version=6.1.2)
* R (tested with version=3.0.0) and packages including
    * [ncdf4][]
    * [raster][]
    * [rasterVis][]

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[netCDF]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[CLM-CN-N2O]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/N2O_emission_inventories_over_AQMEII-NA_2008#!clm-cn-35-dndc-based-process-model
[AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[emis_mole_N2O_200801_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz]: ../../downloads/emis_mole_N2O_200801_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz
[ncdf4]: http://cran.r-project.org/web/packages/ncdf4/
[raster]: http://cran.r-project.org/web/packages/raster/
[rasterVis]: http://cran.r-project.org/web/packages/rasterVis/

# operation

To run this code,

1. `git clone` this repo.
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. Download a copy of [these bash utilities][bash_utilities.sh] to the working directory.
    1. Open the file in in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. You may also want to open the [driver (bash) script][CLMCN_driver.sh] in an editor and take a look. It should run Out Of The Box, but you might need to tweak something there. In the worst case, you could hardcode your paths and apps in the driver.
    1. Once you've got it working, you may want to fork it. If so, you can automate running your changes with [uber_driver.sh][] (changing that if needed, too). If you fork, contribute back: make a pull request!
1. Run the driver:
    `$ ./CLMCN_driver.sh`
        This will download input, then run
    * an [NCL script][reunit.ncl] to convert raw-input concentrations to units appropriate for [CMAQ][CMAQ @ CMAS].
    * an [R script][regrid_global_to_AQMEII.r] to plot the reunit-ed netCDF data, regrid it, and plot the output. The driver should display [an input PDF][global plot] and [an output PDF][AQMEII plot] if properly configured.
    * an [NCL script][retemp.ncl] to "retemporalize" the regridded output into monthly files (since the input is monthly) with hourly timesteps (since at this point we have "flat temporality": we assume emission at constant `mol/s` for each hour of the month).
    * an [NCL script][check_conservation.ncl] to check conservation of mass from input to output. Given that the output domain (AQMEII-NA) is significantly smaller than the input domain (global), it merely reports the fraction of mass (as mgN) in output vs input, and compares that to an estimation of the land area of the output domain relative to the input domain. Current output is

                Is N2O conserved from input to output? units=mgN
                (note (US land area)/(earth land area) ~= 6.15e-02)
                           input      input     output     output           
                month     global        NAs  AQMEII-NA        NAs     out/in
                    1   3.83e+14          0   2.22e+12          0   5.78e-03
                    2   4.24e+14          0   2.33e+12          0   5.49e-03
                    3   4.67e+14          0   3.46e+12          0   7.41e-03
                    4   4.94e+14          0   6.60e+12          0   1.34e-02
                    5   4.94e+14          0   1.02e+13          0   2.06e-02
                    6   6.95e+14          0   1.77e+13          0   2.54e-02
                    7   8.61e+14          0   1.84e+13          0   2.14e-02
                    8   1.01e+15          0   1.74e+13          0   1.73e-02
                    9   8.08e+14          0   1.64e+13          0   2.03e-02
                   10   6.02e+14          0   1.04e+13          0   1.73e-02
                   11   4.72e+14          0   5.01e+12          0   1.06e-02
                   12   4.49e+14          0   3.67e+12          0   8.18e-03

[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[CLMCN_driver.sh]: ../../src/HEAD/CLMCN_driver.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[reunit.ncl]: ../../src/HEAD/reunit.ncl?at=master
[regrid_global_to_AQMEII.r]: ../../src/HEAD/regrid_global_to_AQMEII.r?at=master
[retemp.ncl]: ../../src/HEAD/retemp.ncl?at=master
[check_conservation.ncl]: ../../src/HEAD/check_conservation.ncl?at=master
[CLM-CN-N2O input processing @ project wiki]: https://github.com/TomRoche/cornbeltN2O/wiki/Simulation-of-N2O-Production-and-Transport-in-the-US-Cornbelt-Compared-to-Tower-Measurements#wiki-input-processing-CLM-CNv3.5
[CMAQ @ CMAS]: http://www.cmaq-model.org/
[global plot]: ../../downloads/2008PTONCLMCNN2O_reunit.pdf
[AQMEII plot]: ../../downloads/2008PTONCLMCNN2O_reunit_regrid.pdf

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `get_filepath_from_template.ncl`
    * `string.ncl`
1. Move all these TODOs to [issue tracker][CLM-CN-N2O issues].
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. Create common project for `regrid_resources` à la [regrid_utils][], so I don't hafta hunt down which resource is in which project.
1. All regrids: how to nudge off/onshore as required? e.g., soil or burning emissions should never be offshore, marine emissions should never be onshore..
1. All regrid maps: add Caribbean islands (esp Bahamas! for offshore burning), Canadian provinces, Mexican states.
1. Complain to ncl-talk about NCL "unsupported extensions," e.g., `.ncf` and `<null/>` (e.g., MCIP output).
1. Determine why `<-` assignment is occasionally required in calls to `visualize.*(...)`.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[CLM-CN-N2O issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
