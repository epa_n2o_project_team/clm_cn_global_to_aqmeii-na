;!/usr/bin/env ncl ; requires version >= ???
;;; ----------------------------------------------------------------------
;;; Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

;;; This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

;;; * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;;; * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

;;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
;;; ----------------------------------------------------------------------

; Check conservation of mass in output of retemp/regrid/reunit-ed CLM-CN data relative to input, using input units.

;----------------------------------------------------------------------
; libraries
;----------------------------------------------------------------------

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"    ; all built-ins?
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" ; for int2flt

;----------------------------------------------------------------------
; functions
;----------------------------------------------------------------------

; can't call `getenv` before `begin`?
; can't even load from a string variable?
load "$LAT_LON_FUNCS_FP"
load "$STRING_FUNCS_FP"
load "$TIME_FUNCS_FP"
  
;----------------------------------------------------------------------
; code
;----------------------------------------------------------------------

begin ; skip if copy/paste-ing to console

;----------------------------------------------------------------------
; constants
;----------------------------------------------------------------------

  this_fn = getenv("CALL_CONSERV_FN") ; for debugging

  model_year = stringtoint(getenv("MODEL_YEAR"))
  molar_mass_N2O = stringtofloat(getenv("MOLAR_MASS_N2O"))
  nitrogen_mass_N2O = stringtofloat(getenv("NITROGEN_MASS_N2O"))

  raw_fp = getenv("CLM_CN_RAW_FP")
  raw_datavar_name = getenv("CLM_CN_RAW_DATAVAR_NAME")
  CMAQ_radius = stringtofloat(getenv("CMAQ_RADIUS"))

  ;; load matrix of gridcell map scale factors (MSFs) from GRIDCRO2D_<date/>
  MSF_fp = getenv("MSF_FP") ; path to container netCDF file
  MSF_var_name = getenv("MSF_DATAVAR_NAME")

  out_datavar_name = getenv("CLM_CN_TARGET_DATAVAR_NAME")
  ;;; workaround fact that NCL does not support *.ncf :-(
  output_fp_template_CMAQ = getenv("CLM_CN_TARGET_FP_TEMPLATE_CMAQ")
  output_fp_template_NCL = getenv("CLM_CN_TARGET_FP_TEMPLATE_NCL")
  ; replace output_fp_template_str with <year/><month/> to create "real" filepaths
  output_fp_template_str = getenv("CLM_CN_TARGET_FN_TEMPLATE_STR")

  ; conservation report constants
  sigdigs = stringtoint(getenv("OUTPUT_SIGNIFICANT_DIGITS"))
  report_field_width = stringtoint(getenv("CONSERV_REPORT_FIELD_WIDTH"))
  report_col_sep = getenv("CONSERV_REPORT_COLUMN_SEPARATOR")
  report_title = getenv("CONSERV_REPORT_TITLE")
  report_subtitle = getenv("CONSERV_REPORT_SUBTITLE")
  report_int_format = getenv("CONSERV_REPORT_INT_FORMAT")
  report_flt_format = getenv("CONSERV_REPORT_FLOAT_FORMAT")

;----------------------------------------------------------------------
; payload
;----------------------------------------------------------------------

  ;;; get (global) raw input data
  raw_fh = addfile(raw_fp, "r")  ; file handle
  raw_datavar = raw_fh->$raw_datavar_name$
  ;; ignore bogus "months" 1,14
  ; remember: NCL indexing is {C-style,0-based}
  raw_datavar_arr = raw_datavar(1:12,:,:)
  lat_lon_arr = raw_datavar(0,:,:) ; since only a template, can use any month
  raw_areas = lats_lons_sphere_area(lat_lon_arr, CMAQ_radius)
;  printVarSummary(raw_areas) ; debugging

  ;;; create containers for monthly summaries
  ;; use type=float like n2oemissions(...)
  in_sum = new( (/ 12 /), float)  ; emission sums across domain=global
  in_nNA =  new( (/ 12 /), float) ; count NAs
  out_sum = new( (/ 12 /), float) ; emission sums across domain=AQMEII-NA
  out_nNA =  new( (/ 12 /), float)
  ;; initialize them
  do i_month=0 , 11
    in_sum(i_month) = 0
    in_nNA(i_month) = 0
    out_sum(i_month) = 0
    out_nNA(i_month) = 0
  end do

;   ; TODO: progress control
;   print(str_get_nl()+this_fn+": about to iterate over monthly data")

  ;;; fill the data containers
  do i_month=0 , 11 ; iterate months, C-style indexing

    n_month = i_month + 1

    ;;; process raw input: relatively straightforward, since no unit conversion
    ; units for in_data, in_sum: (mgN/m^2) * m^2
    ; Note multiplication with operator=* is *not* linear-algebra-style!
    ; To do linear-algebra-style matrix multiplication, use operator=#
    in_data = raw_datavar_arr(i_month,:,:) * raw_areas
    in_sum(i_month) = sum(in_data)
    ; count NAs in raw-input month
    in_nNA(i_month) = num(ismissing(in_data))
  
; ; start debugging
; print(str_get_nl() + "in_sum("+i_month+")=="+in_sum(i_month))
; print("in_nNA("+i_month+")=="+in_nNA(i_month))
; ;   end debugging

    ;;; process final outputs
    ;; open monthly output file, noting NCL does not support *.ncf :-(
    time_str = sprinti("%4i", model_year) + sprinti("%0.2i", n_month)
    output_fp_monthly_NCL = \
      str_sub_str(output_fp_template_NCL, output_fp_template_str, time_str)
    output_fp_monthly_CMAQ = \
      str_sub_str(output_fp_template_CMAQ, output_fp_template_str, time_str)
    ;; copy *.ncf to *.nc (with which NCL will cope)
    cmd = "cp "+output_fp_monthly_CMAQ+" "+output_fp_monthly_NCL
;     print(this_fn+": about to do: '"+cmd+"'") ; debugging
    system(cmd)
    output_fp_monthly_NCL_fh = addfile(output_fp_monthly_NCL, "r")

    ;; accumulate stats for output
; keep missing value?
;    out_hourly_arr = (/ output_fp_monthly_NCL_fh->$out_datavar_name$ /)
    out_hourly_arr = output_fp_monthly_NCL_fh->$out_datavar_name$

;     printVarSummary(out_hourly_arr) ; debugging

    ;; Gotta convert output units (moles/s) to input mass (mgN),
    ;; with different timesteps:
    ;; input timestep==monthly
    ;; output timestep==hourly
    ; output gridcell/timestep N2O (moles/s/(timestep=hr)/gridcell)
    ;   * seconds_in_month (for model year)
    ;   * nitrogen_mass_N2O
    ;   / molar_mass_N2O
    ; = input gridcell/timestep mass (mgN/(timestep=mo)/gridcell) as computed above
    output_conv_coeff = seconds_in_month(model_year, n_month) * nitrogen_mass_N2O / molar_mass_N2O

    ;; ASSERT: all output hourly emissions in a month are equal
;    for i_hour = 0 , 23 ; ignore CMAQ 25th hour

      ; TODO: check that dimensions match, MSF vs datavar
      out_hourly_arr_raw = out_hourly_arr(0,0,:,:)
      out_hourly_arr_converted = out_hourly_arr_raw * output_conv_coeff
      out_sum(i_month) = sum(out_hourly_arr_converted)
      out_nNA(i_month) = num(ismissing(out_hourly_arr_raw))

;    end do ; iterate hours
  
; ; start debugging
; print("out_sum("+i_month+")=="+out_sum(i_month))
; print("out_nNA("+i_month+")=="+out_nNA(i_month))
; ;   end debugging
      
    ;;; cleanup
    ;; "close" monthly file
    system("rm "+output_fp_monthly_NCL)
    ; gotta delete(...) one at a time?
    ; > NclBuildArray: can not combine character or string types with numeric types
    ; gotta make a list
    delete( [/ \
      n_month, \
      time_str, \
      output_fp_monthly_NCL, \
      output_fp_monthly_CMAQ, \
      output_fp_monthly_NCL_fh, \
      out_hourly_arr, \
      out_hourly_arr_raw, \
      out_hourly_arr_converted \
    /] )

  end do ; iterate months

  ;;; conservation report, with unit conversion
;  print(report_title) ; prints variable information!
  print(str_get_nl()+report_title+"")
;  print(report_subtitle)
  print(report_subtitle+"")
  print(\  ; header1
    pad_blanks_to_length("", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("input", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("input", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("output", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("output", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("", -report_field_width) )
  print(\  ; header2
    pad_blanks_to_length("month", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("global", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("NAs", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("AQMEII-NA", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("NAs", -report_field_width) + report_col_sep +\
    pad_blanks_to_length("out/in", -report_field_width) )
  
; ; start debugging
;   print("report_int_format="+report_int_format)
;   print("report_flt_format="+report_flt_format)
;   print("for i_month=0:")
;   print("  in_sum(i_month)="+in_sum(0))
;   print("  in_nNA(i_month)="+in_nNA(0))
;   print("  out_sum(i_month)="+out_sum(0))
;   print("  out_nNA(i_month)="+out_nNA(0))
;   print("summary line=:")
;   print(\
;     sprintf(report_flt_format, in_sum(0))  + report_col_sep +\
;     sprinti(report_int_format, toint(in_nNA(0)))  + report_col_sep +\
;     sprintf(report_flt_format, out_sum(0)) + report_col_sep +\
;     sprinti(report_int_format, toint(out_nNA(0))) + report_col_sep)
; ;   end debugging

  do i_month=0 , 11 ; iterate months
    month = i_month + 1
    input_emi = in_sum(i_month)
    output_emi = out_sum(i_month)
    input_NAs = in_nNA(i_month)
    output_NAs = out_nNA(i_month)
    print(\
      sprinti(report_int_format, month)             + report_col_sep +\
      sprintf(report_flt_format, input_emi)         + report_col_sep +\
      sprinti(report_int_format, toint(input_NAs))  + report_col_sep +\
      sprintf(report_flt_format, output_emi)        + report_col_sep +\
      sprinti(report_int_format, toint(output_NAs)) + report_col_sep +\
      sprintf(report_flt_format, output_emi/input_emi) )
  end do ; iterate months
  print(str_get_nl())

; Is N2O conserved from input to output? units=mgN
; (note (US land area)/(earth land area) ~= 6.15e-02)
;                input      input     output     output           
;     month     global        NAs  AQMEII-NA        NAs     out/in
;         1   3.83e+14          0   7.08e+12          0   1.85e-02
;         2   4.24e+14          0   7.39e+12          0   1.74e-02
;         3   4.67e+14          0   1.12e+13          0   2.39e-02
;         4   4.94e+14          0   2.17e+13          0   4.38e-02
;         5   4.94e+14          0   3.45e+13          0   6.97e-02
;         6   6.95e+14          0   6.10e+13          0   8.78e-02
;         7   8.61e+14          0   6.36e+13          0   7.39e-02
;         8   1.01e+15          0   5.90e+13          0   5.87e-02
;         9   8.08e+14          0   5.47e+13          0   6.76e-02
;        10   6.02e+14          0   3.44e+13          0   5.70e-02
;        11   4.72e+14          0   1.65e+13          0   3.49e-02
;        12   4.49e+14          0   1.19e+13          0   2.66e-02

end ; check_conservation.ncl
