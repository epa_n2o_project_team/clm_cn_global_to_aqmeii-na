#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# description
# ----------------------------------------------------------------------

# A top-level driver for creating an inventory of N2O emissions from natural/unmanaged soils over the AQMEII-NA domain from the CLM-CN global inventory:

# * converts global/unprojected CLM-CN data to AQMEII-NA, an LCC-projected subdomain
# * converts monthly to hourly data (over each month)
# * checks conservation of mass over the conversions

# See https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na

# Requirements include:

# * definitely

# ** bash: required to run this script. Known to work with bash --version==3.2.25

# * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), you will need

# ** git: to clone their repo
# ** web access (HTTP currently)

#   These must be available to the script when initially run.

# * ultimately

# ** basename, dirname
# ** cp
# ** curl or wget (latter {preferred, coded} for ability to deal with redirects)
# ** gunzip, unzip
# ** ncdump
# ** NCL
# ** R

# TODO: failing functions should fail this (entire) driver!

# Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(dirname ${THIS})"

# workspace
export WORK_DIR="$(pwd)" # keep it simple for now: same dir as top of repo

### downloading
WGET_TO_FILE='wget --no-check-certificate -c -O'
CURL_TO_FILE='curl -C - -o'
CURL_TO_STREAM='curl'

# model/inventory constants
export MODEL_YEAR='2008'
export NOMINAL_GRIDCELL_AREA='1.440e8' # in meter^2, is a float ; stringtofloat(getenv("NOMINAL_GRIDCELL_AREA"))
export MOLAR_MASS_N2O='44.0128'    # grams per mole of N2O, per wolframalpha.com
export NITROGEN_MASS_N2O='28.0134' # grams per mole of N2,  per wolframalpha.com
export SECONDS_PER_HOUR='3600'     # 60 * 60
export CMAQ_RADIUS='6370000'       # in meters
# where this is hosted
PROJECT_WEBSITE='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na'

### AQMEII-NA constants

## datavar attributes
# TODO: get from template file or from EPIC
# "real" data
AQMEIINA_DV_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_DV_LONG_NAME="$(printf '%-16s' ${AQMEIINA_DV_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_DV_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
# Don't single-quote the payload: double-quote it (OK inside parens inside double-quotes)
AQMEIINA_DV_VAR_DESC="$(printf '%-80s' "Model species ${AQMEIINA_DV_NAME}")"

## "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
AQMEIINA_TFLAG_NAME='TFLAG'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_TFLAG_LONG_NAME="$(printf '%-16s' ${AQMEIINA_TFLAG_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_TFLAG_UNITS="$(printf '%-16s' '<YYYYDDD,HHMMSS>')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
AQMEIINA_TFLAG_VAR_DESC="$(printf '%-80s' 'Timestep-valid flags:  (1) YYYYDDD or (2) HHMMSS')"

## dimensions and their attributes
AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_LAYER_LONG_NAME='index of layers above surface'
AQMEIINA_DIM_LAYER_UNITS='unitless'

AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_TSTEP_UNITS='' # they vary!
AQMEIINA_DIM_TSTEP_LONG_NAME='timestep'

# dimensions we don't really need, but VERDI/IOAPI/TFLAG does
AQMEIINA_DIM_DATETIME_NAME='DATE-TIME'
AQMEIINA_DIM_VAR_NAME='VAR'

AQMEIINA_DIM_X_N='459'
AQMEIINA_DIM_X_NAME='COL'
# AQMEIINA_DIM_X_UNITS='unitless' # my invention
AQMEIINA_DIM_X_UNITS='m'
# AQMEIINA_DIM_X_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_X_LONG_NAME='grid-center offset from center of projection'

AQMEIINA_DIM_Y_N='299'
AQMEIINA_DIM_Y_NAME='ROW'
# AQMEIINA_DIM_Y_UNITS='unitless' # my invention
AQMEIINA_DIM_Y_UNITS='m'
# AQMEIINA_DIM_Y_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_Y_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

### for visualization (generally)
export OUTPUT_SIGNIFICANT_DIGITS='3' # see conservation report below
# PROJ.4 string for unprojected data
export GLOBAL_PROJ4='+proj=longlat +ellps=WGS84'

## for plotting (specifically)
# PDF_VIEWER='xpdf' # set this in bash_utilities::setup_apps
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"
# dimensions (for R plots--units?) for single-frame plots
export SINGLE_FRAME_PLOT_HEIGHT='10'
export SINGLE_FRAME_PLOT_WIDTH='15'
# dimensions for multi-frame plots
export TWELVE_MONTH_PLOT_HEIGHT='120'
export TWELVE_MONTH_PLOT_WIDTH='20'

## conservation report constants
# need 5 additional digits for float output, e.g., "4.85e+04"
# bash arithmetic gotcha: allow no spaces around '='!
# export CONSERV_REPORT_FIELD_WIDTH=$((OUTPUT_SIGNIFICANT_DIGITS + 5))
export CONSERV_REPORT_FIELD_WIDTH='9' # width of 'AQMEII-NA'
export CONSERV_REPORT_FLOAT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}.$((OUTPUT_SIGNIFICANT_DIGITS - 1))e"
# echo -e "${THIS_FN}: CONSERV_REPORT_FLOAT_FORMAT=${CONSERV_REPORT_FLOAT_FORMAT}" # debugging
export CONSERV_REPORT_INT_FORMAT="%${CONSERV_REPORT_FIELD_WIDTH}i"
export CONSERV_REPORT_COLUMN_SEPARATOR="  "
export CONSERV_REPORT_TITLE="Is N2O conserved from input to output? units=mgN"
export CONSERV_REPORT_SUBTITLE="(note (US land area)/(earth land area) ~= 6.15e-02)"

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export CALL_REUNIT_FN='reunit.ncl'
export CALL_REUNIT_FP="${WORK_DIR}/${CALL_REUNIT_FN}"
export CALL_REGRID_FN='regrid_global_to_AQMEII.r'
export CALL_REGRID_FP="${WORK_DIR}/${CALL_REGRID_FN}"
export CALL_RETEMP_FN='retemp.ncl'
export CALL_RETEMP_FP="${WORK_DIR}/${CALL_RETEMP_FN}"
export CALL_CONSERV_FN='check_conservation.ncl'
export CALL_CONSERV_FP="${WORK_DIR}/${CALL_CONSERV_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use   'git@bitbucket.org:tlroche/regrid_utils' if supported
# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${REGRID_UTILS_DIR}/${BASH_UTILS_FN}"
# no, allow user to override repo code: see `get_bash_utils`
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export FILEPATH_FUNCS_FN='get_filepath_from_template.ncl'
export FILEPATH_FUNCS_FP="${WORK_DIR}/${FILEPATH_FUNCS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

export LAT_LON_FUNCS_FN='lats_lons_sphere_area.ncl'
export LAT_LON_FUNCS_FP="${WORK_DIR}/${LAT_LON_FUNCS_FN}"

export STATS_FUNCS_FN='netCDF.stats.to.stdout.r'
export STATS_FUNCS_FP="${WORK_DIR}/${STATS_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

export SUMMARIZE_FUNCS_FN='summarize.ncl'
export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

export VIS_FUNCS_FN='visualization.r'
export VIS_FUNCS_FP="${WORK_DIR}/${VIS_FUNCS_FN}"

# ----------------------------------------------------------------------
# inputs
# ----------------------------------------------------------------------

### raw input

# Raw input data is global/unprojected CLM-CN netCDF, with coordvars=
# lat==(-90,+90)
# lon==(0,+360), hence need to
export ROTATE_INPUT='true'

# time: 14 months (not "decimal year" per `time:units`:
#       month[0]== month[1], month[12]== month[13] (per Saikawa)
# Get from my repository.
CLM_CN_RAW_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/2008PTONCLMCNN2O.nc'
CLM_CN_RAW_FN="$(basename ${CLM_CN_RAW_URI})"
CLM_CN_RAW_FN_ROOT="${CLM_CN_RAW_FN%.*}" # everything left of the dot
export CLM_CN_RAW_FP="${WORK_DIR}/${CLM_CN_RAW_FN}"

# I don't currently plot these
# CLM_CN_RAW_PDF_FN="${CLM_CN_RAW_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
# export CLM_CN_RAW_PDF_FP="${WORK_DIR}/${CLM_CN_RAW_PDF_FN}" # path to PDF output

# TODO: automate getting this metadata from the netCDF
export CLM_CN_RAW_DATAVAR_NAME='n2oemissions'
export CLM_CN_RAW_DATAVAR_LONGNAME='N2O emissions'
export CLM_CN_RAW_DATAVAR_UNIT='mgN/m2/month'
# export CLM_CN_RAW_DATAVAR_NA='-999.0' # not used--hopefully they have no missing data
export CLM_CN_RAW_X_COORDVAR_NAME='lon'
export CLM_CN_RAW_Y_COORDVAR_NAME='lat'
export CLM_CN_RAW_TIME_COORDVAR_NAME='time'

### Template for both `raster` extents and IOAPI-writing

# This file should have all required IOAPI metadata (including the "fake datavar"=TFLAG) for a single datavar=N2O.
# We'll use it to output IOAPI, so we can check grid alignment in VERDI.
# It must also have the correct AQMEII-NA extents, to use in `raster` regridding.
TEMPLATE_IOAPI_GZ_URI='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil/downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz' # final yearly output from AQMEII_ag_soil, VERDI-viewable
# formerly also used
# TEMPLATE_EXTENT_GZ_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/emis_mole_all_20080101_12US1_cmaq_cb05_soa_2008ab_08c.EXTENTS_INPUT.nc.gz'
# et al
TEMPLATE_IOAPI_GZ_FN="$(basename ${TEMPLATE_IOAPI_GZ_URI})"
TEMPLATE_IOAPI_GZ_FP="${WORK_DIR}/${TEMPLATE_IOAPI_GZ_FN}"
# TEMPLATE_IOAPI_ROOT="${TEMPLATE_IOAPI_GZ_FN%.*}" # everything left of the LAST dot
# TEMPLATE_IOAPI_FN="${TEMPLATE_IOAPI_ROOT}"
# that will collide with this project's final output! so instead
TEMPLATE_IOAPI_FN='aqmeii_ag_soil_yearly.nc'
export TEMPLATE_IOAPI_FP="${WORK_DIR}/${TEMPLATE_IOAPI_FN}"
export TEMPLATE_IOAPI_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export TEMPLATE_IOAPI_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export TEMPLATE_IOAPI_BAND='1' # since position of TSTEP in N2O(TSTEP, LAY, ROW, COL)? seems to work

### map scale factors (MSFs)
# load MSFs from some GRIDCRO2D_<date/> which is pretty large, so gzip it

MSF_GZ_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/GRIDCRO2D_080101.gz'
MSF_GZ_FN="$(basename ${MSF_GZ_URI})"
MSF_FN_ROOT="${MSF_GZ_FN%.*}" # everything left of the dot
MSF_FN="${MSF_FN_ROOT}"       # does not normally have an extension
MSF_GZ_FP="${WORK_DIR}/${MSF_GZ_FN}"
export MSF_FP="${WORK_DIR}/${MSF_FN}"
export MSF_DATAVAR_NAME='MSFX2'

# ----------------------------------------------------------------------
# intermediate products
# ----------------------------------------------------------------------

### same CLM-CN netCDF, but in mass-rate units

CLM_CN_REUNIT_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/2008PTONCLMCNN2O_reunit.nc'
CLM_CN_REUNIT_FN="$(basename ${CLM_CN_REUNIT_URI})"
CLM_CN_REUNIT_FN_ROOT="${CLM_CN_REUNIT_FN%.*}" # everything left of the dot
export CLM_CN_REUNIT_FP="${WORK_DIR}/${CLM_CN_REUNIT_FN}"

export CLM_CN_REUNIT_DATAVAR_NAME='N2O' # how CMAQ likes it
# NOTE ON `printf`: don't single-quote the last argument unless it's explicit!
# IOAPI pads varattr=long_name to length=16 with trailing spaces
export CLM_CN_REUNIT_DATAVAR_LONG_NAME="$(printf '%-16s' "${CLM_CN_REUNIT_DATAVAR_NAME}")"
# IOAPI pads varattr=units to length=16 with trailing spaces
export CLM_CN_REUNIT_DATAVAR_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
export CLM_CN_REUNIT_DATAVAR_VAR_DESC="$(printf '%-80s' "Model species ${CLM_CN_REUNIT_DATAVAR_NAME}")"
export CLM_CN_REUNIT_X_COORDVAR_NAME="${CLM_CN_RAW_X_COORDVAR_NAME}"
export CLM_CN_REUNIT_Y_COORDVAR_NAME="${CLM_CN_RAW_Y_COORDVAR_NAME}"
# since we hack off the bogus months ...
export CLM_CN_REUNIT_TIME_COORDVAR_NAME='month'

## visualize it
CLM_CN_REUNIT_PDF_FN="${CLM_CN_REUNIT_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
export CLM_CN_REUNIT_PDF_FP="${WORK_DIR}/${CLM_CN_REUNIT_PDF_FN}" # path to PDF output

### netCDF regridded to AQMEII-NA

CLM_CN_REGRID_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/2008PTONCLMCNN2O_reunit_regrid.nc'
CLM_CN_REGRID_FN="$(basename ${CLM_CN_REGRID_URI})"
CLM_CN_REGRID_FN_ROOT="${CLM_CN_REGRID_FN%.*}" # everything left of the dot
export CLM_CN_REGRID_FP="${WORK_DIR}/${CLM_CN_REGRID_FN}"

# should not this metadata be the same as the template?
export CLM_CN_REGRID_DATAVAR_NAME="${CLM_CN_REUNIT_DATAVAR_NAME}"
# IOAPI coordvars
export CLM_CN_REGRID_X_COORDVAR_NAME='COL'
export CLM_CN_REGRID_Y_COORDVAR_NAME='ROW'
export CLM_CN_REGRID_LAYER_COORDVAR_NAME='LAY'
export CLM_CN_REGRID_TIME_COORDVAR_NAME='TSTEP'
export CLM_CN_REGRID_TIME_COORDVAR_UNIT="month of ${MODEL_YEAR}"

CLM_CN_REGRID_PDF_FN="${CLM_CN_REGRID_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
export CLM_CN_REGRID_PDF_FP="${WORK_DIR}/${CLM_CN_REGRID_PDF_FN}" # path to PDF output

# ----------------------------------------------------------------------
# final product(s)
# ----------------------------------------------------------------------

### create data container files from template file

# problem: NCL currently (version=6.1.2) does not support *.ncf :-( workaround below
# replace a string with values @ runtime
# TODO: driver must ensure output_fp_template does not exist!

# export CLM_CN_TARGET_FN_TEMPLATE_STR='######' # works for NCL, not bash
export CLM_CN_TARGET_FN_TEMPLATE_STR='@@@@@@@@'
CLM_CN_TARGET_FN_ROOT_TEMPLATE="emis_mole_N2O_${CLM_CN_TARGET_FN_TEMPLATE_STR}_12US1_cmaq_cb05_soa_2008ab_08c"
CLM_CN_TARGET_EXT_CMAQ='ncf'
CLM_CN_TARGET_EXT_NCL='nc'
CLM_CN_TARGET_FN_TEMPLATE_CMAQ="${CLM_CN_TARGET_FN_ROOT_TEMPLATE}.${CLM_CN_TARGET_EXT_CMAQ}"
CLM_CN_TARGET_FN_TEMPLATE_NCL="${CLM_CN_TARGET_FN_ROOT_TEMPLATE}.${CLM_CN_TARGET_EXT_NCL}"
export CLM_CN_TARGET_FP_TEMPLATE_CMAQ="${WORK_DIR}/${CLM_CN_TARGET_FN_TEMPLATE_CMAQ}"
export CLM_CN_TARGET_FP_TEMPLATE_NCL="${WORK_DIR}/${CLM_CN_TARGET_FN_TEMPLATE_NCL}"

## global metadata for both {template, "real" monthly/hourly data container} files
export CLM_CN_TARGET_FILE_ATTR_HISTORY="see ${PROJECT_WEBSITE}"
export CLM_CN_TARGET_FILE_ATTR_FILEDESC='monthly N2O emissions from natural soils over AQMEII-NA in mol/s, IOAPI-zed'
# IOAPI metadata: see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
export IOAPI_TIMESTEP_LENGTH='10000' # :TSTEP
export IOAPI_START_OF_TIMESTEPS='0'  # :STIME

## metadata for datavar in both {template, "real" monthly/hourly data container} files
export CLM_CN_TARGET_DATAVAR_NAME="${CLM_CN_REGRID_DATAVAR_NAME}"
# IOAPI pads varattr=long_name to length=16 with spaces
export CLM_CN_TARGET_DATAVAR_ATTR_LONG_NAME="$(printf '%-16s' "${CLM_CN_TARGET_DATAVAR_NAME}")"
# IOAPI pads varattr=units to length=16 with spaces
export CLM_CN_TARGET_DATAVAR_ATTR_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with spaces
export CLM_CN_TARGET_DATAVAR_ATTR_VAR_DESC="$(printf '%-80s' "Model species ${CLM_CN_TARGET_DATAVAR_NAME}")"
## coordvar metadata
export CLM_CN_TARGET_X_COORDVAR_NAME="${CLM_CN_REGRID_X_COORDVAR_NAME}"
export CLM_CN_TARGET_Y_COORDVAR_NAME="${CLM_CN_REGRID_Y_COORDVAR_NAME}"
export CLM_CN_TARGET_LAYER_COORDVAR_NAME="${CLM_CN_REGRID_LAYER_COORDVAR_NAME}"
export CLM_CN_TARGET_TIME_COORDVAR_NAME="${CLM_CN_REGRID_TIME_COORDVAR_NAME}"
export CLM_CN_TARGET_TIME_COORDVAR_UNIT="${CLM_CN_REGRID_TIME_COORDVAR_UNIT}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# TODO: test for resources, reuse if available
# {setup_paths, setup_apps} isa bash util, so `get_helpers` first
function setup {
  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'setup_paths' \
    'setup_apps' \
    'setup_resources' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 1
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
} # end function setup

function get_helpers {
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_filepath_funcs' \
    'get_IOAPI_funcs' \
    'get_lat_lon_funcs' \
    'get_stats_funcs' \
    'get_string_funcs' \
    'get_summarize_funcs' \
    'get_time_funcs' \
    'get_vis_funcs' \
    'get_reunit' \
    'get_regrid' \
    'get_retemp' \
    'get_conserv' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 2
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
} # end function get_helpers

# This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REGRID_UTILS_DIR not defined"
    exit 3
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 4
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 5
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 6
  fi  
} # end function get_regrid_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: BASH_UTILS_FP not defined"
    exit 5
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 6
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${FILEPATH_FUNCS_FP} before running this script.
function get_filepath_funcs {
  if [[ -z "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: FILEPATH_FUNCS_FP not defined"
    exit 7
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${FILEPATH_FUNCS_FN} ${FILEPATH_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: FILEPATH_FUNCS_FP=='${FILEPATH_FUNCS_FP}' not readable"
    exit 8
  fi
} # end function get_filepath_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: IOAPI_FUNCS_FP not defined"
    exit 7
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 8
  fi
} # end function get_IOAPI_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${LAT_LON_FUNCS_FP} before running this script.
function get_lat_lon_funcs {
  if [[ -z "${LAT_LON_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: LAT_LON_FUNCS_FP not defined"
    exit 7
  fi
  if [[ ! -r "${LAT_LON_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${LAT_LON_FUNCS_FN} ${LAT_LON_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${LAT_LON_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: LAT_LON_FUNCS_FP=='${LAT_LON_FUNCS_FP}' not readable"
    exit 8
  fi
} # end function get_lat_lon_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STATS_FUNCS_FP} before running this script.
function get_stats_funcs {
  if [[ -z "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: STATS_FUNCS_FP not defined"
    exit 9
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STATS_FUNCS_FN} ${STATS_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP=='${STATS_FUNCS_FP}' not readable"
    exit 10
  fi
} # end function get_stats_funcs

function get_string_funcs {
  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: STRING_FUNCS_FP not defined"
    exit 11
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 12
  fi
} # end function get_string_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${SUMMARIZE_FUNCS_FP} before running this script.
function get_summarize_funcs {
  if [[ -z "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: SUMMARIZE_FUNCS_FP not defined"
    exit 13
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${SUMMARIZE_FUNCS_FN} ${SUMMARIZE_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP=='${SUMMARIZE_FUNCS_FP}' not readable"
    exit 14
  fi
} # end function get_summarize_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: TIME_FUNCS_FP not defined"
    exit 15
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 16
  fi
} # end function get_time_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${VIS_FUNCS_FP} before running this script.
function get_vis_funcs {
  if [[ -z "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: VIS_FUNCS_FP not defined"
    exit 17
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${VIS_FUNCS_FN} ${VIS_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_FUNCS_FP=='${VIS_FUNCS_FP}' not readable"
    exit 18
  fi
} # end function get_vis_funcs

function get_reunit {
  if [[ -z "${CALL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CALL_REUNIT_FP not defined"
    exit 19
  fi
  # is in this repo
#  if [[ ! -r "${CALL_REUNIT_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_REUNIT_FP} ${REUNIT_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REUNIT_FP=='${CALL_REUNIT_FP}' not readable"
    exit 20
  fi
} # end function get_reunit

function get_regrid {
  if [[ -z "${CALL_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CALL_REGRID_FP not defined"
    exit 21
  fi
  # is in this repo
#  if [[ ! -r "${CALL_REGRID_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_REGRID_FP} ${REGRID_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_REGRID_FP=='${CALL_REGRID_FP}' not readable"
    exit 22
  fi
} # end function get_regrid

function get_retemp {
  if [[ -z "${CALL_RETEMP_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CALL_RETEMP_FP not defined"
    exit 23
  fi
  # is in this repo
#  if [[ ! -r "${CALL_RETEMP_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_RETEMP_FP} ${RETEMP_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_RETEMP_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_RETEMP_FP=='${CALL_RETEMP_FP}' not readable"
    exit 24
  fi
} # end function get_retemp

function get_conserv {
  if [[ -z "${CALL_CONSERV_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CALL_CONSERV_FP not defined"
    exit 25
  fi
  # is in this repo
#  if [[ ! -r "${CALL_CONSERV_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CALL_CONSERV_FP} ${CONSERV_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CALL_CONSERV_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CALL_CONSERV_FP=='${CALL_CONSERV_FP}' not readable"
    exit 25
  fi
} # end function get_conserv

function setup_resources {
  for CMD in \
    'get_raw_input' \
    'get_template_IOAPI' \
    'get_regrid' \
    'get_MSFs' \
    'get_target_template' \
  ; do
    if [[ -z "${CMD}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${CMD}' not defined"
      exit 27
    else
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    fi
  done
} # end function setup_resources

### get the (nearly) raw input
function get_raw_input {
  if [[ -z "${CLM_CN_RAW_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CLM_CN_RAW_FP not defined"
    exit 28
  fi
  if [[ ! -r "${CLM_CN_RAW_FP}" ]] ; then
    for CMD in \
      "${WGET_TO_FILE} ${CLM_CN_RAW_FP} ${CLM_CN_RAW_URI}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${CLM_CN_RAW_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: cannot read CLM_CN_RAW_FP=='${CLM_CN_RAW_FP}'"
    exit 29
  fi
} # end function get_raw_input

### get the "template" file used for regridding and IOAPI-writing
function get_template_IOAPI {
  if [[ -z "${TEMPLATE_IOAPI_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TEMPLATE_IOAPI_FP not defined"
    exit 8
  fi
  if [[ ! -r "${TEMPLATE_IOAPI_FP}" ]] ; then
    if [[ ! -r "${TEMPLATE_IOAPI_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${TEMPLATE_IOAPI_GZ_FP} ${TEMPLATE_IOAPI_GZ_URI}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
    if [[ -r "${TEMPLATE_IOAPI_GZ_FP}" ]] ; then
      # JIC ${TEMPLATE_IOAPI_FP} != ${TEMPLATE_IOAPI_GZ_FP} - .gz
      for CMD in \
        "gunzip -c ${TEMPLATE_IOAPI_GZ_FP} > ${TEMPLATE_IOAPI_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
  fi
  if [[ ! -r "${TEMPLATE_IOAPI_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read TEMPLATE_IOAPI_FP=='${TEMPLATE_IOAPI_FP}'"
    exit 9
  fi
} # end function get_template_IOAPI

### get the regridded output from the R script? no:
### only for testing. TODO: provide commandline-switch control for this
function get_regrid {
  if [[ -z "${CLM_CN_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CLM_CN_REGRID_FP not defined"
    exit 32
  fi
#   if [[ ! -r "${CLM_CN_REGRID_FP}" ]] ; then
#     for CMD in \
#       "${WGET_TO_FILE} ${CLM_CN_REGRID_FP} ${CLM_CN_REGRID_URI}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
#   if [[ ! -r "${CLM_CN_REGRID_FP}" ]] ; then
#     echo -e "${THIS_FN}: ERROR: cannot read CLM_CN_REGRID_FP=='${CLM_CN_REGRID_FP}'"
#     exit 33
#   fi
} # function get_regrid

function get_MSFs {
  if [[ -z "${MSF_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: MSF_FP not defined"
    exit 34
  fi
  if [[ ! -r "${MSF_FP}" ]] ; then
    if [[ ! -r "${MSF_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${MSF_GZ_FP} ${MSF_GZ_URI}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
    if [[ -r "${MSF_GZ_FP}" ]] ; then
      for CMD in \
        "gunzip ${MSF_GZ_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
  fi
  if [[ ! -r "${MSF_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: cannot read MSF_FP=='${MSF_FP}'"
    exit 35
  fi
} # end function get_MSFs

function get_target_template {
  if [[ -z "${CLM_CN_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${THIS_FN}: ERROR: CLM_CN_TARGET_FP_TEMPLATE_NCL not defined"
    exit 36
  fi
  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    # delete: NCL will recreate
    for CMD in \
      "rm ${CLM_CN_TARGET_FP_TEMPLATE_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${THIS_FN}: ERROR: cannot delete CLM_CN_TARGET_FP_TEMPLATE_NCL=='${CLM_CN_TARGET_FP_TEMPLATE_NCL}'"
    exit 37
  fi
} # function get_target_template

### "Reunit" data from flux rate to molar-mass rate prior to regridding.
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function reunit {
#   eval "rm ${CLM_CN_REUNIT_FP}"
#   ncl # bail to NCL and copy script lines
  if [[ -r "${CLM_CN_REUNIT_FP}" ]] ; then
    echo -e "${THIS_FN}: reunit-ed data=='${CLM_CN_REUNIT_FP}' exists, skipping"
#     echo -e "${THIS_FN}: WARNING: deleting CLM_CN_REUNIT_FP=='${CLM_CN_REUNIT_FP}' (should be recreated)"
#     for CMD in \
#       "rm ${CLM_CN_REUNIT_FP}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
#   if [[ -r "${CLM_CN_REUNIT_FP}" ]] ; then
#     echo -e "${THIS_FN}: ERROR: cannot delete CLM_CN_REUNIT_FP=='${CLM_CN_REUNIT_FP}'"
#     exit 38
#   fi
  else
    for SCRIPT in \
        "${CALL_REUNIT_FP}" \
        ; do
        cat <<EOM

About to run NCL script="${SCRIPT}"

EOM
        # '-n' -> http://www.ncl.ucar.edu/Document/Functions/Built-in/print.shtml
        CMD="ncl -n ${SCRIPT}"
        echo -e "$ ${CMD}"
        eval "${CMD}"
    done
    if [[ ! -r "${CLM_CN_REUNIT_FP}" ]] ; then
      echo -e "${THIS_FN}: ERROR: failed to create reunit-ed input=='${CLM_CN_REUNIT_FP}'"
      exit 39
    fi
  fi
} # end function reunit

### regrid from global/unprojected to AQMEII/LCC
function regrid {
#   # ... just start R ...
#   R
#   # ... and maybe source ${CALL_REGRID_FP}, e.g.,
#   source('....r')

  if [[ -r "${CLM_CN_REGRID_FP}" ]] ; then
    echo -e "${THIS_FN}: regridded data=='${CLM_CN_REGRID_FP}' exists, skipping"
  else
    for CMD in \
      "Rscript ${CALL_REGRID_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
    done

    # After exiting R, show cwd and display output PDF ... maybe
    if [[ ! -r "${CLM_CN_REGRID_FP}" ]] ; then
        echo -e "${THIS_FN}: ERROR: failed to create regrid output='${CLM_CN_REGRID_PDF_FP}'"
        exit 40
    fi

    if [[ -r "${CLM_CN_REUNIT_PDF_FP}" ]] ; then
      for CMD in \
          "ls -alht ${CLM_CN_REUNIT_PDF_FP}" \
          "${PDF_VIEWER} ${CLM_CN_REUNIT_PDF_FP} &" \
          ; do
          echo -e "$ ${CMD}"
          eval "${CMD}"
      done
    else
      echo -e "${THIS_FN}: ERROR: failed to plot reunit-ed input='${CLM_CN_REUNIT_PDF_FP}'"
    fi

    if [[ -r "${CLM_CN_REGRID_PDF_FP}" ]] ; then
      for CMD in \
          "ls -alht ${CLM_CN_REGRID_PDF_FP}" \
          "${PDF_VIEWER} ${CLM_CN_REGRID_PDF_FP} &" \
          ; do
          echo -e "$ ${CMD}"
          eval "${CMD}"
      done
    else
      echo -e "${THIS_FN}: ERROR: failed to plot regrid output='${CLM_CN_REGRID_PDF_FP}'"
    fi
  fi
} # end function regrid

### "Retemporalize" from annual to hourly (and put in CMAQ-style file), then check mass conservation.
### TODO: pass commandline args to NCL
### punt: just use envvars :-(
function retemp_conserv {
#   eval "rm ${CLM_CN_TARGET_FP_TEMPLATE_NCL}"
#   ncl # bail to NCL and copy script lines

  for SCRIPT in \
    "${CALL_RETEMP_FP}" \
    "${CALL_CONSERV_FP}" \
  ; do
    cat <<EOM

About to run NCL script="${SCRIPT}"

EOM
    # '-n' -> http://www.ncl.ucar.edu/Document/Functions/Built-in/print.shtml
    CMD="ncl -n ${SCRIPT}"
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done

  ### cleanup

  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_CMAQ}" ]] ; then
    for CMD in \
      "rm ${CLM_CN_TARGET_FP_TEMPLATE_CMAQ}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_CMAQ}" ]] ; then
    echo -e "${THIS_FN}: ERROR: cannot delete CLM_CN_TARGET_FP_TEMPLATE_CMAQ=='${CLM_CN_TARGET_FP_TEMPLATE_CMAQ}'"
    exit 41
  fi

  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    for CMD in \
      "rm ${CLM_CN_TARGET_FP_TEMPLATE_NCL}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ -r "${CLM_CN_TARGET_FP_TEMPLATE_NCL}" ]] ; then
    echo -e "${THIS_FN}: ERROR: cannot delete CLM_CN_TARGET_FP_TEMPLATE_NCL=='${CLM_CN_TARGET_FP_TEMPLATE_NCL}'"
    exit 42
  fi

} # end function retemp_conserv

function teardown {
  ### show files: data and plots
  for CMD in \
    "ls -alht ${WORK_DIR}/*.pdf" \
    "ls -alht ${WORK_DIR}/*.${CLM_CN_TARGET_EXT_CMAQ}" \
    "ls -alht ${WORK_DIR}/*.${CLM_CN_TARGET_EXT_NCL}" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
} # end function teardown

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# should always
# * begin with `setup` setup paths, apps, helpers, resources
# * end with `teardown` for tidy and testing (e.g., plot display)
#   'setup' \
#   'reunit' \
#   'regrid' \
#   'retemp_conserv' \
#   'teardown' \
for CMD in \
  'setup' \
  'reunit' \
  'regrid' \
  'retemp_conserv' \
  'teardown' \
; do
  if [[ -z "${CMD}" ]] ; then
    echo -e "${THIS_FN}::main loop: ERROR: '${CMD}' not defined"
    exit 43
  else
    echo -e "\n$ ${THIS_FN}::main loop: ${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
