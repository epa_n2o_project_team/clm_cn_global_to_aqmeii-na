### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# R code to 2D-regrid CLM-CN global/unprojected netCDF data to AQMEII-NA, an LCC-projected subdomain

# If running manually in R console, remember to run setup actions: `source ./regrid_global_to_AQMEII.sh`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

## data

# kludge for my clumsy namespacing
my_this_fn <- Sys.getenv('CALL_REGRID_FN')
this_fn <- my_this_fn

# all the following env vars must be set and exported in driver script
work_dir <- Sys.getenv('WORK_DIR')
pdf_er <- Sys.getenv('PDF_VIEWER')
sigdigs <- as.numeric(Sys.getenv('OUTPUT_SIGNIFICANT_DIGITS'))

in_fp <- Sys.getenv('CLM_CN_REUNIT_FP')
raster_rotate <- as.logical( Sys.getenv('ROTATE_INPUT'))
# in_band <- Sys.getenv('CLM_CN_REUNIT_BAND')
in_datavar_name <- Sys.getenv('CLM_CN_REUNIT_DATAVAR_NAME')
in_datavar_longname <- Sys.getenv('CLM_CN_REUNIT_DATAVAR_LONGNAME')
in_datavar_unit <- Sys.getenv('CLM_CN_REUNIT_DATAVAR_UNIT')
# in_datavar_na <- as.numeric(Sys.getenv('CLM_CN_REUNIT_DATAVAR_NA'))
in_x_coordvar_name <- Sys.getenv('CLM_CN_REUNIT_X_COORDVAR_NAME')
in_y_coordvar_name <- Sys.getenv('CLM_CN_REUNIT_Y_COORDVAR_NAME')
in_z_coordvar_name <- Sys.getenv('CLM_CN_REUNIT_TIME_COORDVAR_NAME')
in_z_coordvar_unit <- Sys.getenv('CLM_CN_REUNIT_TIME_COORDVAR_UNIT')

in_pdf_fp <- Sys.getenv('CLM_CN_REUNIT_PDF_FP')
in_pdf_height <- as.numeric(Sys.getenv('TWELVE_MONTH_PLOT_HEIGHT'))
in_pdf_width <- as.numeric(Sys.getenv('TWELVE_MONTH_PLOT_WIDTH'))

template_datavar_name <- Sys.getenv('TEMPLATE_DATAVAR_NAME')
template_in_fp <- Sys.getenv('TEMPLATE_IOAPI_FP')

out_fp <- Sys.getenv('CLM_CN_REGRID_FP')
out_datavar_name <- in_datavar_name
out_datavar_longname <- in_datavar_longname
out_datavar_unit <- in_datavar_unit
# out_datavar_na <- in_datavar_na
out_x_coordvar_name <- Sys.getenv('CLM_CN_REGRID_X_COORDVAR_NAME')
out_y_coordvar_name <- Sys.getenv('CLM_CN_REGRID_Y_COORDVAR_NAME')
out_z_coordvar_name <- Sys.getenv('CLM_CN_REGRID_TIME_COORDVAR_NAME')
out_z_coordvar_unit <- Sys.getenv('CLM_CN_REGRID_TIME_COORDVAR_UNIT')

out_pdf_fp <- Sys.getenv('CLM_CN_REGRID_PDF_FP')
out_pdf_height <- as.numeric(Sys.getenv('TWELVE_MONTH_PLOT_HEIGHT'))
out_pdf_width <- as.numeric(Sys.getenv('TWELVE_MONTH_PLOT_WIDTH'))

stat_script_fp <- Sys.getenv('STATS_FUNCS_FP')
viz_funcs_fp <- Sys.getenv('VIS_FUNCS_FP')
global_proj4 <- Sys.getenv('GLOBAL_PROJ4')

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

source(stat_script_fp) # in script, produces errant error=
#> netCDF.stats.to.stdout.r: no arguments supplied, exiting
source(viz_funcs_fp)

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup
# ----------------------------------------------------------------------

# accelerate R graphics over SSH, per Adam Wilson
# http://planetflux.adamwilson.us/2012/03/r-graphics-via-ssh.html
X11.options(type="Xlib")

# coordinate reference system:
# use package=M3 to get CRS from template file
library(M3)
out.proj4 <- M3::get.proj.info.M3(template_in_fp)
# cat(sprintf('out.proj4=%s\n', out.proj4)) # debugging
# out.proj4=+proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000
out.crs <- sp::CRS(out.proj4)
global.crs <- sp::CRS(global_proj4)

# ----------------------------------------------------------------------
# (minimally) process input
# ----------------------------------------------------------------------

library(raster)
# in.raster <- raster::raster(in.fp, varname=raw.datavar.name, band=in.band)
in.raster <- raster::brick(in_fp, varname=in_datavar_name)
# replace the layer names
in.raster.layers.n <- length(names(in.raster))
names(in.raster) <- as.character(c(1:in.raster.layers.n))
# correct zero-based longitudes. TODO: test first!
if (raster_rotate) {
  in.raster <- rotate(
    in.raster,      # )
    overwrite=TRUE) # else levelplot does one layer per page?
}

# # start debugging
# in.raster
# # class       : RasterBrick 
# # dimensions  : 96, 144, 13824, 12  (nrow, ncol, ncell, nlayers)
# # resolution  : 2.5, 1.875  (x, y)
# # extent      : -180, 180, -90, 90  (xmin, xmax, ymin, ymax)
# # coord. ref. : +proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0 
# # data source : in memory
# # names       :       X1,       X2,       X3,       X4,       X5,       X6,       X7,       X8,       X9,      X10,      X11,      X12 
# # min values  :        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0,        0 
# # max values  : 15315.48, 19092.71, 16271.40, 18353.34, 12817.79, 23656.67, 31079.63, 60260.55, 20473.18, 20044.36, 17812.99, 30699.73 
# #             : 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 

# summary(in.raster) # compare to netCDF.stats following
# #                 [,1]         [,2]         [,3]         [,4]         [,5]
# # Min.    0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # 1st Qu. 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # Median  0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # 3rd Qu. 2.443989e-07 7.944394e-08 1.243423e-06 3.940473e-04 4.601156e-03
# # Max.    1.531548e+04 1.909271e+04 1.627140e+04 1.835334e+04 1.281779e+04
# # NA's    0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# #                 [,6]         [,7]         [,8]         [,9]        [,10]
# # Min.    0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # 1st Qu. 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # Median  0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# # 3rd Qu. 3.152941e-02 8.006553e-02 7.813510e-02 4.539829e-02 1.587790e-02
# # Max.    2.365667e+04 3.107963e+04 6.026055e+04 2.047318e+04 2.004436e+04
# # NA's    0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00
# #                [,11]        [,12]
# # Min.    0.000000e+00 0.000000e+00
# # 1st Qu. 0.000000e+00 0.000000e+00
# # Median  0.000000e+00 0.000000e+00
# # 3rd Qu. 5.641609e-06 7.941855e-07
# # Max.    1.781299e+04 3.069973e+04
# # NA's    0.000000e+00 0.000000e+00
# #   end debugging

# ----------------------------------------------------------------------
# visualize input
# ----------------------------------------------------------------------

# "create" world map
library(maps)
map.world.unproj <- maps::map('world', plot=FALSE)
map.world.unproj.shp <-
  maptools::map2SpatialLines(map.world.unproj, proj4string=global.crs)
# summary(map.world.unproj.shp) # debugging

# Why does '=' fail and '<-' succeed in the arg list?
visualize.layers(
  nc.fp=in_fp,
  brick=in.raster,
  datavar.name=in_datavar_name,
  layer.dim.name=in_z_coordvar_name,
  sigdigs=sigdigs,
#  map.list=list(map.world.unproj.shp),
  map.list <- list(map.world.unproj.shp),
  pdf.fp=in_pdf_fp,
  pdf.height=in_pdf_height,
  pdf.width=in_pdf_width
)

# globe displays normally, dim=lon-lat
# interesting Asian spikes 6-9

# ----------------------------------------------------------------------
# regrid
# ----------------------------------------------------------------------

# use M3 to get extents from template file (thanks CGN!)
extents.info <- M3::get.grid.info.M3(template_in_fp)
extents.xmin <- extents.info$x.orig
extents.xmax <- max(
  M3::get.coord.for.dimension(
    file=template_in_fp, dimension="col", position="upper", units="m")$coords)
extents.ymin <- extents.info$y.orig
extents.ymax <- max(
  M3::get.coord.for.dimension(
    file=template_in_fp, dimension="row", position="upper", units="m")$coords)
grid.res <- c(extents.info$x.cell.width, extents.info$y.cell.width) # units=m

template.extents <-
  raster::extent(extents.xmin, extents.xmax, extents.ymin, extents.ymax)
# template.extents # debugging
# class       : Extent 
# xmin        : -2556000 
# xmax        : 2952000 
# ymin        : -1728000 
# ymax        : 1860000 

template.in.raster <-
#  raster::raster(template_in_fp, varname=template_datavar_name, band=template_band)
  raster::raster(template_in_fp, varname=template_datavar_name)
template.raster <- raster::projectExtent(template.in.raster, crs=out.crs)
#> Warning message:
#> In projectExtent(template.in.raster, out.proj4) :
#>   158 projected point(s) not finite
# is that "projected point(s) not finite" warning important? Probably not, per Hijmans

# without this, extents aren't correct!
template.raster@extent <- template.extents
# should resemble the domain specification @
# https://github.com/TomRoche/cornbeltN2O/wiki/AQMEII-North-American-domain#wiki-EPA

# template.raster # debugging
# class       : RasterLayer 
# dimensions  : 299, 459, 137241  (nrow, ncol, ncell)
# resolution  : 12000, 12000  (x, y)
# extent      : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
# coord. ref. : +proj=lcc +lat_1=33 +lat_2=45 +lat_0=40 +lon_0=-97 +a=6370000 +b=6370000 

# at last: do the regridding
out.raster <-
  raster::projectRaster(
    # give a template with extents--fast, but gotta calculate extents
    from=in.raster, to=template.raster, crs=out.crs,
    # give a resolution instead of a template? no, that hangs
#    from=in.raster, res=grid.res, crs=out.proj4,
    method='bilinear', overwrite=TRUE, format='CDF',
    # args from writeRaster
#    NAflag=out_datavar_na,
    varname=out_datavar_name, 
    varunit=out_datavar_unit,
    longname=out_datavar_longname,
    xname=out_x_coordvar_name,
    yname=out_y_coordvar_name,
    zname=out_z_coordvar_name,
    zunit=out_z_coordvar_unit,
    filename=out_fp)
# Warning message:
# In rgdal:::.gd_transform(projfrom, projto, nrow(xy), xy[, 1], xy[,  :
#   49 projected point(s) not finite

# ----------------------------------------------------------------------
# visualize output
# ----------------------------------------------------------------------

## get projected North American map
NorAm.shp <- project.NorAm.boundaries.for.CMAQ(
  units='m',
  extents.fp=template_in_fp,
  extents=template.extents,
  LCC.parallels=c(33,45),
  CRS=out.crs)

# # start debugging
# class(NorAm.shp)
# # [1] "SpatialLines"
# # attr(,"package")
# # [1] "sp"
# bbox(NorAm.shp)
# #         min     max
# # x -11407761 3236868
# # y  -2879888 5942516

# # compare bbox to (above)
# # > template.extents
# # class       : Extent 
# # xmin        : -2556000 
# # xmax        : 2952000 
# # ymin        : -1728000 
# # ymax        : 1860000 
# #   end debugging

# Why does '=' fail and '<-' succeed in the arg list?
visualize.layers(
  nc.fp=out_fp,
  brick=out.raster,
  datavar.name=out_datavar_name,
  layer.dim.name=out_z_coordvar_name,
  sigdigs=sigdigs,
#  map.list=list(map.noram.shp.proj, state.map.IOAPI.shp),
#  map.list <- list(map.noram.shp.proj, state.map.IOAPI.shp),
  map.list <- list(NorAm.shp),
  pdf.fp=out_pdf_fp,
  pdf.height=out_pdf_height,
  pdf.width=out_pdf_width
)
